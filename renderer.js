const TILE_RADIUS_X = 96;
const TILE_RADIUS_Y = 27;

let cameraX = 0, cameraY = 0;
let cameraScale = 0.3;

let selectedX, selectedY;
let selectedStructure;
let buildMode = 'place'; // 'place' or 'demolish';

const BUTTON_PADDING = 50;
const BUTTON_SIZE = 80;

let DEBUG_TILES = true;
let DEBUG_HITBOXES = false;
let DEBUG_TASKS = false;
let DEBUG_PATHFINDING = false;

function initRenderer() {
    createCanvas(windowWidth, windowHeight);
    angleMode(RADIANS);
}

function preload() {
    IMAGE_WORKER = loadImage('images/worker.png');
    IMAGE_WORKER_SOUTH_1 = loadImage('images/worker-south-1.png');
    IMAGE_WORKER_SOUTH_2 = loadImage('images/worker-south-2.png');
    IMAGE_WORKERS = new Array(IMAGE_WORKER, IMAGE_WORKER_SOUTH_1, IMAGE_WORKER_SOUTH_2);

    IMAGE_FARMLAND = loadImage('images/farmland-plain.png');
    IMAGE_SILO = loadImage('images/silo.png');

    IMAGE_BUTTON_DEMOLISH = loadImage('images/demolish.png');
}

function render() {
    background(255);
    push();
    scale(cameraScale, cameraScale);
    translate((windowWidth / 2 - cameraX) / cameraScale, (windowHeight / 2 - cameraY) / cameraScale);
    currentRegion.render();
    pop();

    // draw GUI
    image(IMAGE_BUTTON_DEMOLISH, BUTTON_PADDING, windowHeight - BUTTON_PADDING - BUTTON_SIZE, BUTTON_SIZE, BUTTON_SIZE);
}

function windowResized() {
    resizeCanvas(windowWidth, windowHeight);
}

function tileToCoordsX(tileX, tileY) {
    return (tileX + tileY) * TILE_RADIUS_X;
}
function tileToCoordsY(tileX, tileY) {
    return (tileX - tileY) * TILE_RADIUS_Y;
}

function coordsToTileX(coordsX, coordsY) {
    return floor(1/2 * (coordsX / TILE_RADIUS_X + coordsY / TILE_RADIUS_Y + 1));
}
function coordsToTileY(coordsX, coordsY) {
    return floor(1/2 * (coordsX / TILE_RADIUS_X - coordsY / TILE_RADIUS_Y + 1));
}

function screenToCoordsX(screenX, screenY) {
    return (screenX - windowWidth / 2 + cameraX) / cameraScale;
}
function screenToCoordsY(screenX, screenY) {
    return (mouseY - windowHeight / 2 + cameraY) / cameraScale;
}
