class Region {

    constructor(radius) {
        this.minTileX = -radius;
        this.minTileY = -radius;
        this.maxTileX = radius;
        this.maxTileY = radius;

        this.structureTiles = new Map();
        this.structures = new Set();

        this.entityTiles = new Map();
        this.entities = new Set();

        this.selection = new Set();
    }

    render() {
        if (DEBUG_TILES || DEBUG_PATHFINDING) {
            strokeWeight(3);
            noFill();
            stroke(255, 204, 0);
            for (let x = this.minTileX; x <= this.maxTileX; x++) {
                for (let y = this.minTileY; y <= this.maxTileY; y++) {
                    if (DEBUG_PATHFINDING) {
                        let obstacle = this.isObstacle(x, y);
                        if (!obstacle) fill(0, 0, 100);
                        else           fill(0, 0, 0);
                    }
                    this.renderTileQuad(x, y);
                }
            }
        }

        // @performance
        // render structures and entities
        let sortedObjects = Array.from(this.structures).concat(Array.from(this.entities));
        sortedObjects.sort((a, b) => (a.depthY - b.depthY));
        for (let object of sortedObjects) {
            this.renderObject(object);
        }

        // buildMode
        if (buildMode === 'place' && selectedStructure) {
            strokeWeight(3);
            noFill();
            stroke(255, 100, 0);
            for (let x = selectedX; x < selectedX + selectedStructure.tileWidth; x++) {
                for (let y = selectedY;  y < selectedY + selectedStructure.tileHeight; y++) {
                    if (this.containsTile(x, y)) {
                        if (this.containsStructure(x, y)) {
                            stroke(255, 100, 0);
                        } else {
                            stroke(100, 255, 0);
                        }
                        this.renderTileQuad(x, y);
                    }
                }
            }
        } else if (buildMode === 'demolish') {
            fill(255, 0, 0, 50);
            stroke(255, 100, 0);
            let structure = this.getStructureAt(selectedX, selectedY);
            if (structure) {
                structure.renderTileQuads();
            }
        }
    }

    renderTileQuad(tileX, tileY) {
        let x = tileToCoordsX(tileX, tileY);
        let y = tileToCoordsY(tileX, tileY);
        // text('x' + tileX + 'y' + tileY, x, y);
        quad(x, y - TILE_RADIUS_Y, x + TILE_RADIUS_X, y, x, y + TILE_RADIUS_Y, x - TILE_RADIUS_X, y);
    }

    renderObject(object) {
        let noObstacle = object instanceof Structure && !object.obstacle;

        if (noObstacle) {
            object.render();
        }
        // render selection
        if (this.selection.has(object)) {
            stroke(200, 0, 0);
            strokeWeight(3);
            noFill();
            ellipse(tileToCoordsX(object.centerX(), object.centerY()), tileToCoordsY(object.centerX(), object.centerY()),
                TILE_RADIUS_X * (object.tileWidth * 2 - 1), TILE_RADIUS_Y * (object.tileHeight * 2 - 1));
        }
        if (!noObstacle) {
            object.render();
        }
    }

    update() {
        for (let structure of this.structures) {
            structure.update();
        }
        for (let entity of this.entities) {
            entity.update();
        }
    }

    containsTile(tileX, tileY) {
        return tileX >= this.minTileX && tileX <= this.maxTileX && tileY >= this.minTileY && tileY <= this.maxTileY;
    }

    containsTiles(tileX, tileY, tileWidth, tileHeight) {
        return tileX >= this.minTileX && tileX + tileHeight - 1 <= this.maxTileX
            && tileY >= this.minTileY && tileY + tileWidth - 1  <= this.maxTileY;
    }

    addStructure(structure) {
        this.structures.add(structure);
        for (let tileX = structure.tileX; tileX < structure.tileX + structure.tileWidth; tileX++) {
            for (let tileY = structure.tileY; tileY < structure.tileY + structure.tileHeight; tileY++) {
                this.structureTiles.set(tileX + ':' + tileY, structure);
            }
        }
    }

    removeStructure(structure) {
        this.structures.delete(structure);
        for (let tileX = structure.tileX; tileX < structure.tileX + structure.tileWidth; tileX++) {
            for (let tileY = structure.tileY; tileY < structure.tileY + structure.tileHeight; tileY++) {
                this.structureTiles.delete(tileX + ':' + tileY);
            }
        }
    }

    containsStructure(tileX, tileY, tileWidth, tileHeight) {
        if (!tileWidth)  tileWidth = 1;
        if (!tileHeight) tileHeight = 1;

        for (let x = tileX; x < tileX + tileWidth; x++) {
            for (let y = tileY; y < tileY + tileHeight; y++) {
                if (this.structureTiles.has(x + ':' + y)) {
                    return true;
                }
            }
        }
        return false;
    }

    getStructureAt(tileX, tileY) {
        return this.structureTiles.get(tileX + ':' + tileY);
    }

    addEntity(entity) {
        this.entities.add(entity);
        this.addEntityPosition(entity, entity.tileX, entity.tileY);
    }

    addEntityPosition(entity, nextX, nextY) {
        this.entityTiles.set(nextX + ':' + nextY, entity);
    }

    removeEntityPosition(entity, lastX, lastY) {
        this.entityTiles.delete(lastX + ':' + lastY, entity);
    }

    removeEntity(entity) {
        this.entities.delete(entity);
        this.removeEntityPosition(entity, entity.tileX, entity.tileY);
    }

    getEntityAt(tileX, tileY) {
        return this.entityTiles.get(tileX + ':' + tileY);
    }

    containsEntity(tileX, tileY, tileWidth, tileHeight) {
        if (!tileWidth)  tileWidth = 1;
        if (!tileHeight) tileHeight = 1;

        for (let x = tileX; x < tileX + tileWidth; x++) {
            for (let y = tileY; y < tileY + tileHeight; y++) {
                if (this.entityTiles.has(x + ':' + y)) {
                    return true;
                }
            }
        }
        return false;
    }


    isObstacle(tileX, tileY) {
        if (!this.containsTile(tileX, tileY)) {
            return true;
        }
        let structure = this.getStructureAt(tileX, tileY)
        if (structure && structure.obstacle) {
            return true;
        }
        let entity = this.getEntityAt(tileX, tileY);
        if (entity) {
            return true;
        }
        return false;
    }

    containsObstacle(tileX, tileY, tileWidth, tileHeight) {
        if (!tileWidth)  tileWidth = 1;
        if (!tileHeight) tileHeight = 1;

        for (let x = tileX; x < tileX + tileWidth; x++) {
            for (let y = tileY; y < tileY + tileHeight; y++) {
                if (this.isObstacle(x, y)) {
                    return true;
                }
            }
        }
        return false;
    }
}
