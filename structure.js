class Structure extends GameObject {
    constructor(region, tileX, tileY, tileWidth, tileHeight, image, obstacle) {
        super(region, tileX, tileY, tileWidth, tileHeight, image);
        this.obstacle = obstacle;

        if (this.obstacle) {
            // depth is in middle
            this.depthX = tileToCoordsX(this.tileX + (this.tileWidth - 1) /2, this.tileY + (this.tileHeight - 1) /2);
            this.depthY = tileToCoordsY(this.tileX + (this.tileWidth - 1) / 2, this.tileY + (this.tileHeight - 1) / 2);
        } else {
            // depth is in back
            this.depthX = tileToCoordsX(this.tileX - 1/2, this.tileY + (this.tileHeight - 1) + 1/2);
            this.depthY = tileToCoordsY(this.tileX - 1/2, this.tileY + (this.tileHeight - 1) + 1/2);
        }
    }

    renderTileQuads() {
        for (let tileX = this.tileX; tileX < this.tileX + this.tileWidth; tileX++) {
            for (let tileY = this.tileY; tileY < this.tileY + this.tileHeight; tileY++) {
                this.region.renderTileQuad(tileX, tileY);
            }
        }
    }

    renderImage() {
        let img = this.image;

        let x = tileToCoordsX(this.tileX + (this.tileWidth - 1) / 2, this.tileY + (this.tileHeight - 1) / 2);
        let y = tileToCoordsY(this.tileX + this.tileWidth - 1, this.tileY);

        image(img, x - img.width / 2, y - img.height + 3 * TILE_RADIUS_Y);

        if (DEBUG_HITBOXES) {
            fill(200, 0, 80);
            ellipse(x, y, 10, 10);
            fill(200, 200, 80);
            ellipse(this.depthX, this.depthY, 10, 10);
        }
    }

    render() {
        if (DEBUG_TILES) {
            fill(150);
            this.renderTileQuads();
        }
        this.renderImage();
    }

    update() {
    }
}

BUILDABLE_STRUCTURES = {
    'farmland': {
        tileWidth: 2, tileHeight: 2,
        obstacle: false,
        make: (region, tileX, tileY) => (new Farmland(region, tileX, tileY))
    }, 'silo': {
        tileWidth: 4, tileHeight: 4,
        obstacle: true,
        make: (region, tileX, tileY) => (new Silo(region, tileX, tileY))
    }, 'worker': {
        tileWidth: 1, tileHeight: 1,
        obstacle: true,
        make: (region, tileX, tileY) => {
            let i = Math.floor(Math.random() * IMAGE_WORKERS.length);
            return new Entity(region, tileX, tileY, IMAGE_WORKERS[i]);
        },
        entity: true
    }
}
