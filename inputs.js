let shiftDown = false;

function mouseDragged() {
    if (!currentRegion) return;
    // move camera
    if (mouseButton === LEFT) {
        let diffX = pmouseX - mouseX;
        let diffY = pmouseY - mouseY;

        cameraX = cameraX + diffX;
        cameraY = cameraY + diffY;

        let minX = tileToCoordsX(currentRegion.minTileX, currentRegion.minTileY);
        let maxX = tileToCoordsX(currentRegion.maxTileX, currentRegion.maxTileY);
        let minY = tileToCoordsY(currentRegion.minTileX, currentRegion.maxTileY);
        let maxY = tileToCoordsY(currentRegion.maxTileX, currentRegion.minTileY);
        cameraX = max(min(cameraX, maxX), minX);
        cameraY = max(min(cameraY, maxY), minY);
    }
}

function mouseWheel(event) {
    cameraScale = max(min(cameraScale - event.delta * 0.0005, 1), 0.05);
}

function mouseMoved() {
    let cursorX = screenToCoordsX(mouseX, mouseY);
    let cursorY = screenToCoordsY(mouseX, mouseY);

    let tileX = coordsToTileX(cursorX, cursorY);
    let tileY = coordsToTileY(cursorX, cursorY);

    let gridSnap = 1;
    if (selectedStructure) {
        gridSnap = min(min(selectedStructure.tileWidth, selectedStructure.tileHeight), 2);
    }

    selectedX = floor(tileX / gridSnap) * gridSnap;
    selectedY = floor(tileY / gridSnap) * gridSnap;
}

function mousePressed() {
    if (!currentRegion) return;

    if (mouseButton === LEFT) {
        if (BUTTON_PADDING <= mouseX && mouseX < BUTTON_PADDING + BUTTON_SIZE
            && windowHeight - BUTTON_PADDING - BUTTON_SIZE <= mouseY && mouseY < windowHeight - BUTTON_PADDING) {
            // demolish button!
            if (buildMode === 'place') {
                buildMode = 'demolish';
            } else {
                buildMode = 'place';
            }
            return;
        }

        if (buildMode === 'place' && selectedStructure) {
            if (selectedStructure.entity) {
                // temporary entity place hack!
                if (!currentRegion.containsObstacle(selectedX, selectedY, selectedStructure.tileWidth, selectedStructure.tileHeight)) {
                    let entity = selectedStructure.make(currentRegion, selectedX, selectedY);
                    currentRegion.addEntity(entity);
                    if (!currentRegion.selection.size || shiftDown) {
                        currentRegion.selection.add(entity);
                    }
                }
            } else {
                if (currentRegion.containsTiles(selectedX, selectedY, selectedStructure.tileWidth, selectedStructure.tileHeight)
                    && !currentRegion.containsStructure(selectedX, selectedY, selectedStructure.tileWidth, selectedStructure.tileHeight)
                    && !(selectedStructure.obstacle && currentRegion.containsEntity(selectedX, selectedY, selectedStructure.tileWidth, selectedStructure.tileHeight))) {
                    let structure = selectedStructure.make(currentRegion, selectedX, selectedY);
                    currentRegion.addStructure(structure);
                    if (!currentRegion.selection.size || shiftDown) {
                        currentRegion.selection.add(structure);
                    }
                }
            }
        } else if (buildMode === 'demolish') {
            let structure = currentRegion.getStructureAt(selectedX, selectedY);
            if (structure) {
                currentRegion.removeStructure(structure);
            } else {
                // temporary entity destroy hack!
                let entity = currentRegion.getEntityAt(selectedX, selectedY);
                if (entity) {
                    currentRegion.removeEntity(entity);
                    if (currentRegion.selection.has(entity)) {
                        currentRegion.selection.delete(entity);
                    }
                }
            }
        }
    } else if (mouseButton === RIGHT) {
        // goto task
        for (entity of currentRegion.selection) {
            if (entity instanceof Entity) {
                let task = new MoveToTask(entity, selectedX, selectedY);
                entity.addTask(task, !shiftDown);
            }
        }
    }
}

const STRUCTURE_HOTKEYS = [ 'worker', 'farmland', 'silo' ];

function keyTyped() {
    let numKey = Number(key);
    if (0 <= numKey && numKey < STRUCTURE_HOTKEYS.length) {
        selectedStructure = BUILDABLE_STRUCTURES[STRUCTURE_HOTKEYS[numKey]];
    }
    buildMode = 'place';


    if (key === 't') {
        DEBUG_TILES = !DEBUG_TILES;
    }
    if (key === 'h') {
        DEBUG_HITBOXES = !DEBUG_HITBOXES;
    }
    if (key === 'z') {
        DEBUG_TASKS = !DEBUG_TASKS;
    }
    if (key === 'p') {
        DEBUG_PATHFINDING = !DEBUG_PATHFINDING;
    }
}

function keyPressed() {
    if (keyCode === SHIFT) {
        shiftDown = true;
    }
}

function keyReleased() {
    if (keyCode === SHIFT) {
        shiftDown = false;
    }
}
