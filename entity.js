
class Entity extends GameObject {
    constructor(region, tileX, tileY, image) {
        super(region, tileX, tileY, 1, 1, image);

        this.setPosition(tileX, tileY);
        this.baseSpeed = 5;

        this.taskList = new Array();
    }

    renderImage() {
        let img = this.image;

        let x = tileToCoordsX(this.tileX, this.tileY);
        let y = tileToCoordsY(this.tileX, this.tileY);

        image(img, x - img.width / 2, y - img.height + 2 * TILE_RADIUS_Y);

        if (DEBUG_HITBOXES) {
            fill(200, 0, 80);
            ellipse(x, y, 10, 10);
            fill(200, 200, 80);
            ellipse(this.depthX, this.depthY, 10, 10);
        }
    }

    render() {

        this.renderImage();

        let currentTask = this.getCurrentTask();
        if (currentTask) {
            currentTask.render();
        }

    }

    update() {
        let busy = false;
        // when you walk, or do something else that cannot be interrupted

        // movement
        if (this.isMoving()) {
            this.moveProgress = min(this.moveProgress + this.moveSpeed, 1);
            if (this.moveProgress < 1) {
                busy = true;
            } else {
                // finished walking
                this.region.removeEntityPosition(this, this.lastX, this.lastY);
                this.setPosition(this.nextX, this.nextY);
            }
        }

        this.tileX = this.lastX + (this.nextX - this.lastX) * this.moveProgress;
        this.tileY = this.lastY + (this.nextY - this.lastY) * this.moveProgress;

        this.depthX = tileToCoordsX(this.tileX + 1/4, this.tileY - 1/4);
        this.depthY =  tileToCoordsY(this.tileX + 1/4, this.tileY - 1/4);

        // choose new tasks! important: do this after updating (positional) properties for this entity!
        if (!busy) {
            let currentTask = this.getCurrentTask();
            if (currentTask) {
                let completed = currentTask.updateEntity(this);
                if (completed) {
                    this.taskList.shift(); // remove first entry
                }
            }
        }
    }

    addTask(task, force) {
        if (force) {
            this.taskList.length = 0; // clear array
        }
        this.taskList.push(task);
    }

    getCurrentTask() {
        if (this.taskList.length !== 0) {
            return this.taskList[0];
        } else {
            return null;
        }
    }

    setPosition(tileX, tileY) {
        this.tileX = tileX;
        this.tileY = tileY;
        this.lastX = tileX;
        this.lastY = tileY;
        this.nextX = tileX;
        this.nextY = tileY;
        this.moveProgress = 0;
    }

    isMoving() {
        return this.lastX != this.nextX || this.lastY != this.nextY;
    }

    moveTo(targetX, targetY, speed) {
        if (this.isMoving()) {
            return false;
        }
        if (this.region.isObstacle(targetX, targetY)) {
            return false;
        }
        this.region.addEntityPosition(this, targetX, targetY);
        this.nextX = targetX;
        this.nextY = targetY;
        this.moveSpeed = speed;
        return true;
    }
}
