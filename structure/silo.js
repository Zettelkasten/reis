class Silo extends Structure {

    constructor(region, tileX, tileY) {
        super(region, tileX, tileY, 4, 4, IMAGE_SILO, true);
    }
}
