class Farmland extends Structure {

    constructor(region, tileX, tileY) {
        super(region, tileX, tileY, 2, 2, IMAGE_FARMLAND, false);
        this.obstacle = false;
    }
}
