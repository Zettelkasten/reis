class EntityTask {
    constructor(entity) {
        this.entity = entity;
    }

    // returns whether task is completed
    updateEntity() {
    }

    render() {
    }
}

class MoveToTask extends EntityTask {

    constructor(entity, targetX, targetY) {
        super(entity);
        this.targetX = targetX;
        this.targetY = targetY;
    }

    updateEntity() {
        let entityX = this.entity.tileX;
        let entityY = this.entity.tileY;

        if (entityX == this.targetX && entityY == this.targetY) {
            // arrived
            return true;
        } else {
            let deltaX = Math.sign(this.targetX - entityX);
            let deltaY = Math.sign(this.targetY - entityY);

            // try to correct path if way is blocked
            if (this.entity.region.isObstacle(entityX + deltaX, entityY + deltaY)) {
                if (!this.entity.region.isObstacle(entityX + deltaX, entityY)) {
                    deltaY = 0;
                } else if (!this.entity.region.isObstacle(entityX, entityY + deltaY)) {
                    deltaX = 0;
                } else {
                    // we're screwed
                    return true;
                }
            }
            let distanceX = tileToCoordsX(deltaX, deltaY);
            let distanceY = tileToCoordsY(deltaX, deltaY);
            let distance = Math.sqrt(distanceX * distanceX + distanceY * distanceY);
            let success = this.entity.moveTo(entityX + deltaX,
                               entityY + deltaY,
                               this.entity.baseSpeed / distance);
            return !success;
        }
    }

    render() {
        if (DEBUG_TASKS) {
            stroke(10);
            color(10, 100, 100);
            line(tileToCoordsX(this.entity.tileX, this.entity.tileY),
                 tileToCoordsY(this.entity.tileX, this.entity.tileY),
                 tileToCoordsX(this.targetX, this.targetY),
                 tileToCoordsY(this.targetX, this.targetY));
        }
    }
}
