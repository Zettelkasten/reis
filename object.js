class GameObject {
    constructor(region, tileX, tileY, tileWidth, tileHeight, image) {
        this.region = region;
        this.tileX = tileX;
        this.tileY = tileY;
        this.tileWidth = tileWidth;
        this.tileHeight = tileHeight;
        this.image = image;
    }

    render() {
    }

    update() {
    }

    centerX() {
        return this.tileX + (this.tileWidth - 1) / 2;
    }

    centerY() {
        return this.tileY + (this.tileHeight - 1) / 2;
    }
}
