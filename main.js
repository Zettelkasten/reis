let currentRegion;

function initGame() {
    currentRegion = new Region(40);
    currentRegion.addStructure(new Silo(currentRegion, 0, 0));
    currentRegion.addStructure(new Silo(currentRegion, 4, 0));

    for (let x = -7; x <= -5; x++) {
        for (let y = -2; y <= 7; y++) {
            currentRegion.addStructure(new Farmland(currentRegion, 2 * x, 2 * y));
        }
    }

    selectedStructure = BUILDABLE_STRUCTURES['farmland'];

    let entity = new Entity(currentRegion, 2, -1, IMAGE_WORKER_SOUTH_1);
    currentRegion.addEntity(entity);
    currentRegion.selection.add(entity);
}

function updateGame() {
    if (currentRegion) {
        currentRegion.update();
    }
}

function setup() {
    initGame();
    initRenderer();
}

function draw() {
    updateGame();
    render();
}
